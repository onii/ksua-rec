#!/bin/bash
today=`date +%Y-%m-%d`
retries=0
#cd /mnt/Sandisk_32GB_USB/SaBM/

rec () {
    curl http://stereo.wavestreamer.com:9169/audio.mp3 >> $1_"$today".mp3 &
    pget=$!
    sleep $2 && kill $pget &
    wait $pget
    # Wait until download exits
    if [ $? != 143 ]; then
        # If this happens, we got a bad exit code
        ((retries++))
        if [ $retries == 60 ]; then
            echo "This is pointless. I'm giving up."
            exit 1
        else
            echo "Stream broke - trying again. Attempt: $retries"
            sleep 30
            rec $1 $2
        fi
    fi
#    mid3v2 -a "KSUA" -A "KSUA" -t "Strange and Beautiful Music $today" \
#    -y $today $1_"$today".mp3
    # Add metadata
#    avconv -i $1_"$today".mp3 -i /home/username/Pictures/Cover.jpg \
#    -map 0 -map 1 -acodec copy $1_"$today"-image.mp3
    # Add cover artwork
#    file1=$(stat -c%s $1_"$today".mp3)
#    file2=$(stat -c%s $1_"$today"-image.mp3)
#    if [ "$file1" -lt "$file2" ]; then
#        mv $1_"$today"-image.mp3 $1_"$today".mp3
#    else
#        rm $1_"$today"-image.mp3
        # Something went wrong with avconv
#    fi
    exit 0
}

rec "SaBM_-" "2h 10m"

