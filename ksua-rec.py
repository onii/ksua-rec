#!/usr/bin/python
import time, signal, requests, shutil

filename = ("SaBM_-_" + time.strftime("%Y-%m-%d") + ".mp3")
rectime = 7800 # 2h10m in seconds
url = "http://stereo.wavestreamer.com:9169/audio.mp3"

class TimeoutException(Exception):
    pass

def timeout_handler(signum, frame):
    raise TimeoutException

def download_file(url):
    r = requests.get(url, stream=True)
    with open(filename, 'wb') as f:
        shutil.copyfileobj(r.raw, f)
    return filename

signal.signal(signal.SIGALRM, timeout_handler)
signal.alarm(rectime)
try:
    download_file(url)
except:
    pass
