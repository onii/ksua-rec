# ksua-rec.sh

## There's a newer project that replaces this. [Check it out!](https://gitlab.com/onii/internet-radio)

Records audio from the KSUA live stream.
Used to capture Strange and Beautiful Music.

Requires `curl` for bash, or the `requests` module for Python.

Typical cron:

    55 18 * * Sun /home/username/bin/ksua-rec.sh
